
const keys = 'LEFT,RIGHT,UP,DOWN,SPACE,W,A,S,D,R'
let pl, plats, k, bad, coins, enemySpawner, bgmusic

function preload() {
  this.load.image('bg', './assets/img/bigbg.png')
  this.load.image('pl', './assets/img/ship2.png')
  this.load.image('plat', './assets/img/platform.png')
  this.load.image('bad', './assets/img/bad.png')
  this.load.image('coin', './assets/img/coin.png')

  this.load.audio('bgmusic', './assets/snd/bgmusic.wav')
  this.load.audio('bump', './assets/snd/bump.wav')
  this.load.audio('coinup', './assets/snd/coinup.wav')
  this.load.audio('gameover', './assets/snd/gameover.wav')
  this.load.audio('jump', './assets/snd/jump.wav')

}


function create() {
  this.add.image(0, 0, 'bg').setOrigin(0, 0)

  bgmusic = this.sound.add('bgmusic')
  let bump = this.sound.add('bump')
  let coinup = this.sound.add('coinup')
  let gameover = this.sound.add('gameover')
  let jump = this.sound.add('jump')
  bgmusic.play()
  bump.play()
  coinup.play()
  gameover.play()
  jump.play()

  pl = this.physics.add.sprite(100, 100, 'pl')
  pl.setCollideWorldBounds(true)
  pl.setVelocity(2000)
  pl.setBounce(0)
  pl.setScale(.25)
  pl.setGravityY(200)

  let bads = this.physics.add.group()

  const spawnEnemy = () => {
    let b = bads.create(1, 1, 'bad')
    b.setCollideWorldBounds(true)
    b.setVelocity(200)
    b.setBounce(1)
    b.setScale(.10)
    b.setGravity(100)
  }

  spawnEnemy()
  enemySpawner = setInterval(spawnEnemy, 250)


  coin = this.physics.add.sprite(100, 100, 'coin')
  coin.setCollideWorldBounds(true)
  coin.setScale(.05)
  coin.setVelocity(300)
  coin.setBounce(0)





  plats = this.physics.add.staticGroup()
  plats.create(300, 300, "plat")
  plats.create(200, 200, "plat")
  plats.create(400, 400, "plat")

  let c
  coins = this.physics.add.group()
  c = coins.create(20, 20, 'coin')
  c.setScale(.05)
  c.setCollideWorldBounds(true)
  c.setGravityY(1200)

  c = coins.create(200, 20, 'coin')
  c.setScale(.05)
  c.setCollideWorldBounds(true)
  c.setGravityY(1200)



  this.physics.add.collider(pl, plats)
  //this.physics.add.collider(bads, plats)
  this.physics.add.collider(pl, coins)
  this.physics.add.collider(pl, bads)
  this.physics.add.collider(coins, plats)



  k = this.input.keyboard.addKeys(keys)
}

function update() {

  if (k.R.isDown) {
    clearInterval(enemySpawner)
    bgmusic.stop()
    this.scene.restart()
  }


  if (k.LEFT.isDown) {
    pl.setVelocityX(-300)
  }

  else if (k.RIGHT.isDown) {
    pl.setVelocityX(300)
  }

  if (pl.body.onFloor()) {
    if (k.UP.isDown) {
      pl.setVelocityY(-400)
    }
    pl.setDragX(2000)
  }
}



let config = {
  scene: { preload, create, update },
  physics: {
    default: 'arcade',

  },
}

new Phaser.Game(config)
